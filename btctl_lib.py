# ##############
# BTCTL.LIB
# Access to BLUETOOTH via Bluetoothctl and subprocess
#
# Author: Stefan Beck
# 
# Version: 1.00
# Date: 13.01.19
# ##############

import subprocess, time

BTDEVLIST_ID = 0
BTDEVLIST_NAME = 1
BTDEVLIST_PAIRED = 2
BTDEVLIST_TRUSTED = 3
BTDEVLIST_CONNECTED = 4


BTCTL_MAX_SCAN_TIME_S = 90
BTCLT_DEF_SCAN_TIME_S = 30

#Set to True if debug output are necessary
DEBUG_OUTPUT = False

#Scan the Bluetooth-Net for active devices for the specified time (max. BTCTL_MAX_SCAN_TIME s)
def btctl_Scan(time_s = BTCLT_DEF_SCAN_TIME_S):

    if (DEBUG_OUTPUT):
        print ("Scanning for %d s" % time_s)
        
    process = subprocess.Popen(['bluetoothctl'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)

    process.stdin.write("scan on\n")
    process.stdin.flush()
    if (time_s > BTCTL_MAX_SCAN_TIME_S):
        time_s = BTCTL_MAX_SCAN_TIME_S
    time.sleep(time_s)
    process.stdin.write("scan off\n")
    process.stdin.flush()
    process.stdin.write("quit\n")
    process.stdin.flush()
    process.wait()

#Remove the specifc bluetooth device 
def btctl_RemoveDevice(btid):

    if (DEBUG_OUTPUT):
        print ("Remove device %s" % btid)

    process = subprocess.Popen(['bluetoothctl'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)

    # First have to remove complete
    btctl_cmd = "remove %s\n" % btid
    process.stdin.write(btctl_cmd)
    process.stdin.flush()
    time.sleep(2)

    
#Get detailed info of one special bluetooth device
def btctl_GetInfo(btid):
    
    is_found = False
    is_paired = False
    is_trusted = False
    is_connected = False
    
    if (DEBUG_OUTPUT):
        print ("Get info of %s" % btid)
    
    process = subprocess.Popen(['bluetoothctl'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)

    btctl_cmd = "info %s\n" % btid

    process.stdin.write(btctl_cmd)
    process.stdin.flush()
    process.stdin.write("quit\n")
    process.stdin.flush()
    process.wait()

    info_found = False
    device_found = False
    
    for line in process.stdout.readlines():
        if ("# info" in line):
            info_found = True
            word = line.split()
            for part in word:                
                if (part == btid):
                    device_found = True
                    is_found = True
                else:
                    device_found = False
                
        elif (device_found):            
            word = line.split()
            paired_found = False
            trusted_found = False
            connected_found = False
            for part in word:
                if (part.lower() == "paired:"):
                    paired_found = True                
                elif  (part.lower() == "trusted:"):
                    trusted_found = True
                elif (part.lower() == "connected:"):
                    connected_found = True
                elif (paired_found):
                    if (part.lower() == "no"):
                        is_paired = False
                    else:
                        is_paired = True
                elif (trusted_found):
                    if (part.lower() == "no"):
                        is_trusted = False
                    else:
                        is_trusted = True
                elif (connected_found):
                    if (part.lower() == "no"):
                        is_connected = False
                    else:
                        is_connected = True

    return (is_found, is_paired, is_trusted, is_connected)


#Disconnect on specific device
def btctl_DisconnectDevice(btid):

    if (DEBUG_OUTPUT):
        print ("Disconnect device: %s" % btid)

    # Get detailed info for this ID
    is_found, is_paired, is_trusted, is_connected = btctl_GetInfo(btid)

    if (is_found) and (is_connected == True):

        process = subprocess.Popen(['bluetoothctl'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)

        # Try to disconnect
        btctl_cmd = "disconnect %s\n" % btid
        process.stdin.write(btctl_cmd)
        process.stdin.flush()
        time.sleep(5)
        process.stdin.write("quit\n")
        process.stdin.flush()
        process.wait()

#Connect one specific device
def btctl_ConnectDevice(btid):

    if (DEBUG_OUTPUT):
        print ("Connect device: %s" % btid)

    # Get detailed info for this ID
    is_found, is_paired, is_trusted, is_connected = btctl_GetInfo(btid)

    if (is_found) and (is_connected == False):

        process = subprocess.Popen(['bluetoothctl'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)

        # Try to pair
        if (is_paired == False):
            if (DEBUG_OUTPUT):
                print ("Pairing")
            btctl_cmd = "pair %s\n" % btid
            process.stdin.write(btctl_cmd)
            process.stdin.flush()
            time.sleep(3)
        
        # Try to trust
        if (is_trusted == False):
            if (DEBUG_OUTPUT):
                print ("Trusting")
            btctl_cmd = "trust %s\n" % btid
            process.stdin.write(btctl_cmd)
            process.stdin.flush()
            time.sleep(2)

        # Try to connect
        if (DEBUG_OUTPUT):
            print ("Connecting")
        btctl_cmd = "connect %s\n" % btid
        process.stdin.write(btctl_cmd)
        process.stdin.flush()
        time.sleep(10)
        
        process.stdin.write("quit\n")
        process.stdin.flush()
        process.wait()

        is_found, is_paired, is_trusted, is_connected = btctl_GetInfo(btid)
        
    return (is_connected)


#Start bluetooth scan
def btctl_ScanStart():
            
    if (DEBUG_OUTPUT):
        print ("Start Scan")
            
    process = subprocess.Popen(['bluetoothctl'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)

    process.stdin.write("scan on\n")
    process.stdin.flush()
    
    return (process)
    
#Stop bluetooth scan
# <process> has to be the return value of btctl_ScanStart()
def btctl_ScanStop(process):

    if (process.poll() == None):
        process.stdin.write("scan off\n")
        process.stdin.flush()

        process.stdin.write("quit\n")
        process.stdin.flush()
        process.wait()


#Get a list of all known devices with ID, Name and an connected and paired flag    
def btctl_GetDeviceList(with_scan = False):
            
    if (DEBUG_OUTPUT):
        print ("Get devicelist")
        
    #if a scan should be done first
    if (with_scan):
        btctl_Scan()
        
    process = subprocess.Popen(['bluetoothctl'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, universal_newlines=True)

    process.stdin.write("devices\n")
    process.stdin.flush()
    process.stdin.write("quit\n")
    process.stdin.flush()
    process.wait()

    found_device = False
    devicelist = []
    
    for line in process.stdout.readlines():
        if ("# device" in line):
            found_device = True
        elif ("# quit" in line):
            found_device = False
        else:
            if (found_device):
                if ("Device" in line):
                    words = line.split()
                    if (len(words) > 0):
                        dummy = words.pop(0)
                        if (len(words) > 0):
                            id = words.pop(0);
                            name = ""
                            if (len(words) > 0):
                                name = words.pop(0)
                            while (len(words) > 0):
                                name = name + " " + words.pop(0)
                            if (name == ""):
                                name = "Unkown device"
                            else:
                                name.strip()                                                
                                
                            device = [id, name, False, False, False]
                                                        
                            devicelist.append(device)
    
    is_one_device_connected = False
    active_device_id = ""
        
    for device in devicelist:
        
        if (DEBUG_OUTPUT):
            print (device)
        
        
        # Get detailed info for this ID
        is_found, is_paired, is_trusted, is_connected = btctl_GetInfo(device[BTDEVLIST_ID])
        
        if (is_paired):
            device[BTDEVLIST_PAIRED] = True

        if (is_trusted):
            device[BTDEVLIST_TRUSTED] = True
            
        if (is_connected):
            device[BTDEVLIST_CONNECTED] = True
            active_device_id = device[BTDEVLIST_ID]
            is_one_device_connected = True
            
    #Return the final prepared devicelist        
    return (devicelist, is_one_device_connected, active_device_id)

    
    
