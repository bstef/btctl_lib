This small library offers functions to 
- scan for Bluetooth devices
- connect and disconnect a specific Bluetooth device
- get a list of all Bluetooth devices found
- get detailed info of a specific Bluetooth device
- remove a specific Bluetooth device


Requirements:

python 2.7 or newer
subprocess
bluetoothctl

Remark:
bluetoothctl needs the packages "bluez" and "bluez_utils".


I used and tested this library with Ubuntu 18.4 and Raspian 

Last update of this README.md: 18.07.19


Regards,
Stefan

